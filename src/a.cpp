#include <iostream>
#include "a.h"

void a(){
    #ifdef _WIN64
        std::cout << "libA 64 bit\n";
    #else
        std::cout << "libA 32 bit\n";
    #endif
}